// builtin
var fs = require('fs');

// 3rd party
var express = require('express');
var hbs = require('hbs');
const mysql = require('mysql');
var app = express();
var session = require('express-session');
var async = require('async');
const multer = require('multer');
const path = require('path');
const moment = require('moment');


moment.locale('pl');
const conn = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'przepisy'
});
app.use(session({
    secret: '2C44-4D44-WppQ38S',
    resave: true,
    saveUninitialized: true
}));
const storage = multer.diskStorage({
    destination: './public/uploads/',
    filename: function (req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
    }
});

// Init Upload
const uploads = multer({
    storage: storage,
    limits: { fileSize: 1000000 },
    fileFilter: function (req, file, cb) {
        checkFileType(file, cb);
    }
}).array('myImages');
// Check File Type
function checkFileType(file, cb) {
    // Allowed ext
    const filetypes = /jpeg|jpg|png|gif/;
    // Check ext
    const extname = filetypes.test(path.extname(file.originalname).toLowerCase());
    // Check mime
    const mimetype = filetypes.test(file.mimetype);

    if (mimetype && extname) {
        return cb(null, true);
    } else {
        cb('Error: Images Only!');
    }
}
var auth = function (req, res, next) {
    if (req.session && req.session.user === "user" && req.session.admin)
        return next();
    else
        return res.redirect('/');

};
hbs.registerPartial('partial', fs.readFileSync(__dirname + '/views/partial.hbs', 'utf8'));
hbs.registerPartials(__dirname + '/views/partials');

hbs.registerHelper('ifvalue', function (conditional, options) {
    if (options.hash.value === conditional) {
        return options.fn(this)
    } else {
        return options.inverse(this);
    }
});

// set the view engine to use handlebars
app.set('view engine', 'hbs');
app.set('views', __dirname + '/views');

conn.connect((err) => {
    console.log('Mysql Connected...');
});

app.use(express.static(__dirname + '/public'));


app.get('/', function (req, res) {

    let kategoria = req.query.kategoria;

    let search = req.query.search;

    if (kategoria == undefined && search == undefined) { search = ""; }

    var final = {};
    async.series({
        recipe: function (cb) {
            conn.query("SELECT * FROM `przepis`  LEFT JOIN ulubione on (przepis.id = ulubione.id_recipe) WHERE przepis.kategoria='" + kategoria + "' OR przepis.nazwa LIKE '%" + search + "%' ", function (error, result, client) {
                cb(error, result);
            })
        },
    },
        function (error, results) {

           let kategoria = []
            if (req.query.search == undefined) {
                if (results.recipe[0]) { kategoria[0] = { kategoria: results.recipe[0].kategoria } }
                
            }
            else { kategoria[0] = { searched: "Wyszukano: ", kategoria: req.query.search } }
            if (results.recipe[0]) {
                res.render('index', {
                    results: results.recipe,
                    kategoria: kategoria,
                    nameOfUser: global.id_username,
                    user: req.session.admin
                });
            }
            else {
                res.render('index', { user: req.session.admin, nameOfUser: global.id_username }, function (err, html) {
                    res.send(html + '&nbsp &nbsp &nbsp &nbsp Niestety nic nie znaleziono. Spróbuj ponownie.');
                })

            }
        });
});
// 90 endpoint
app.get('/login', function (req, res) {
    var username = req.query.username;
    var password = req.query.password;
    if (username && password) {
        conn.query('SELECT * FROM uzytkownicy WHERE username = ? AND password = ?', [username, password], function (error, results, fields) {

            if (results.length > 0) {
                global.id_user = results[0].id;
                global.id_username = results[0].username;
                req.session.user = "user";
                req.session.admin = true;
                res.redirect('/');
            }
            else {
                let message = "Błędny login lub hasło!";
                res.render('auth', {
                    message: message
                })
            }
        })
    }
});

// Logout endpoint
app.get('/logout', function (req, res) {
    global.id_user = undefined;
    global.id_username = undefined;
    req.session.destroy();
    res.redirect('/');
});

app.get('/editWindow', function (req, res) {
    let sql = "SELECT * FROM przepis WHERE id='" + id + "'";
    let query = conn.query(sql, (err, results) => {
        res.render('edit_recipe', {
            results: results,
            nameOfUser: global.id_username
        });
    });
});

//route for insert data//ZMIANA 27-05
app.post('/save', (req, res) => {
    let sql = "INSERT INTO przepis SET ?";

    uploads(req, res, (err) => {
        let messageName;
        let messageIngredients;
        let messagePrepare;
        let messageCategory;
        let messagePhoto;
        let good;

        let images = [];
        var files = req.files;

        files.forEach(function (el, i) {
            fs.writeFile(`uploads/${el.filename}`, 'binary', function (err) { });
            images[i] = el.filename;
        })
        let firstImg = images[0];
        let img = images.join();

        if (req.body.nazwa == "") {
            messageName = "Podaj nazwę przepisu!";
        }
        if (req.body.skladniki == "") {
            messageIngredients = "Wymień składniki!";
        }
        if (req.body.przygotowanie == "") {
            messagePrepare = "Podaj sposób przygotowania!";
        }
        if (req.body.kategoria == "") {
            messageCategory = "Podaj kategorię!";
        }
        if (firstImg === undefined) {
            messagePhoto = "Wstaw zdjęcie!";
        }
        if (messageName === undefined && messageIngredients === undefined && messagePrepare === undefined && messageCategory === undefined && messagePhoto === undefined) {
            good = "Udało się dodać przepis! Możesz dodać kolejny!"
        }

        let data = { id_user: global.id_user, nazwa: req.body.nazwa, skladniki: req.body.skladniki, przygotowanie: req.body.przygotowanie, kategoria: req.body.kategoria, photo: firstImg, photos: img };

        if (messageName !== undefined || messageIngredients !== undefined || messagePrepare !== undefined || messageCategory !== undefined || messagePhoto !== undefined) {
            res.render('add_recipe', {
                good: good, user: req.session.admin, nameOfUser: global.id_username, messageName: messageName, messageIngredients: messageIngredients, messagePrepare: messagePrepare, messageCategory: messageCategory, messagePhoto: messagePhoto
            })
        } else {
            let query = conn.query(sql, data, (err, results) => {
                res.render('add_recipe', {
                    good: good, user: req.session.admin, nameOfUser: global.id_username
                })
            });
        }
    });
});

app.get('/changeMainPhoto', (req, res) => {
    let sql = "UPDATE przepis SET photo='" + req.query.photo + "' WHERE id=" + global.id;
    let query = conn.query(sql, (err, results) => {
    });
    res.redirect('/recipe');
});
app.get('/saveComment', (req, res) => {
    let sql = "INSERT INTO komentarze SET ?";

    let data = { comment: req.query.comment, id_user: global.id_user, id_recipe: global.id };
    let query = conn.query(sql, data, (err, results) => {
        res.redirect('/recipe');
    });

});


//route for delete data
app.get('/deleteComment', auth, function (req, res) {
    let sql = "DELETE FROM komentarze WHERE id_comment=" + req.query.id_comment + "";
    let query = conn.query(sql, (err, results) => {
        res.redirect('/recipe');
    });
});
app.get('/deleteImg', auth, function (req, res) {

    let sql = "UPDATE przepis SET photos='" + req.query.photos + "' WHERE id=" + global.id;
    let query = conn.query(sql, (err, results) => {
        res.redirect('/edit_recipe');
    });
});
app.post('/saveEditImg', (req, res) => {

    uploads(req, res, (err) => {
        let images = [];
        var files = req.files;

        files.forEach(function (el, i) {
            fs.writeFile(`uploads/${el.filename}`, 'binary', function (err) { });
            images[i] = el.filename;
        })
        let img = "," + images.join();
        let sql = "update przepis set photos =CONCAT(photos, '" + img + "') where id = '" + global.id + "';";
        let query = conn.query(sql, (err, results) => {
            res.redirect('/editWindow');
        });
    });

});
app.get('/updateComment', auth, function (req, res) {
    let sql = "UPDATE komentarze SET comment='" + req.query.comment + "' WHERE id_comment=" + req.query.id_comment;
    let query = conn.query(sql, (err, results) => {
        res.redirect('/recipe');
    });
});
app.get('/editUser', (req, res) => {
    let sql = "SELECT * FROM uzytkownicy WHERE id='" + global.id_user + "'";
    let query = conn.query(sql, (err, results) => {
        res.render('editUser', {
            results: results,
            nameOfUser: global.id_username
        });
    });

});

app.get('/update', auth, function (req, res) {
    let messageName;
    let messageIngredients;
    let messagePrepare;
    let messageCategory;
    if (req.query.nazwa == "") {
        messageName = "Podaj nazwę przepisu!";
    }
    if (req.query.skladniki == "") {
        messageIngredients = "Wymień składniki!";
    }
    if (req.query.przygotowanie == "") {
        messagePrepare = "Podaj sposób przygotowania!";
    }
    if (req.query.kategoria == "") {
        messageCategory = "Podaj kategorię!";
    }
    if (messageName == undefined && messageIngredients == undefined && messagePrepare == undefined && messageCategory == undefined) {
        good = "Udało się edytować przepis!";
        let sql = "UPDATE przepis SET nazwa='" + req.query.nazwa + "', skladniki='" + req.query.skladniki + "', przygotowanie='" + req.query.przygotowanie + "', kategoria='" + req.query.kategoria + "' WHERE id=" + id;
        let query = conn.query(sql, (err, results) => {
            res.redirect('/recipe');
        });
    }
    else {

        let sql = "SELECT * FROM przepis WHERE id='" + id + "'";
        let query = conn.query(sql, (err, results) => {
            res.render('edit_recipe', {
                nameOfUser: global.id_username, messageName: messageName, messageIngredients: messageIngredients, messagePrepare: messagePrepare, messageCategory: messageCategory,
                results: results
            });
        });
    }
});

app.get('/updateUser', auth, function (req, res) {
    let pass = req.query.password;
    let pass2 = req.query.password2;
    let message;
    async.series({
        user: function (cb) {
            conn.query("SELECT * FROM uzytkownicy WHERE id='" + global.id_user + "'", function (error, result, client) {
                cb(error, result);
            })
        },
        userUpdate: function (cb) {
            conn.query("UPDATE uzytkownicy SET password='" + req.query.password + "' WHERE id=" + global.id_user + ";", function (error, result, client) {
                cb(error, result)
            })
        }
    },
        function (error, results) {

            if (pass == pass2 && pass2 == pass) {
                res.redirect('/profile');
            }
            else {
                message = "Hasła nie pokrywają się!";

                res.render('editUser', {
                    results: results.user,
                    user: req.session.admin,
                    nameOfUser: global.id_username,
                    message: message
                });
            }
        });
});

//other user profile
app.get('/otherUserProfile', function (req, res) {

    if (req.query.id_user) {
        global.idThisUser = req.query.id_user;
    }
    var final = {};
    async.series({
        recipe: function (cb) {
            conn.query("SELECT * FROM przepis,uzytkownicy where przepis.id_user='" + global.idThisUser + "' and uzytkownicy.id='" + global.idThisUser + "'  ", function (error, result, client) {
                cb(error, result);
            })
        },
        user: function (cb) {
            conn.query("SELECT * from  uzytkownicy where  uzytkownicy.id='" + global.id_user + "'", function (error, result, client) {
                cb(error, result)
            })
        }
    },
        function (error, results) {

            if (!error && results.recipe.length == 0) {
                return res.render('otherUserProfile', { user: req.session.admin, nameOfUser: global.id_username }, function (err, html) {

                    res.send(html + "&nbsp &nbsp &nbsp &nbsp Ten użytkownik nie dodał żadnego przepisu. ");
                })
            }

            if (!error) {
                let thisUser = { username: results.recipe[0].username }

                res.render('otherUserProfile', {
                    results: results.recipe,
                    thisUser: thisUser,
                    user: results.user,
                    nameOfUser: global.id_username,
                });
            }

        });
});

//route for delete data
app.post('/delete', auth, function (req, res) {

    let sql = "DELETE FROM przepis WHERE id=" + global.id + "";
    let query = conn.query(sql, (err, results) => {
        res.redirect('/');
    });
});

app.get('/openadd', auth, function (req, res) {
    res.render('add_recipe', {
        nameOfUser: global.id_username
    });
});
app.get('/recipe', function (req, res) {
    global.id;
    if (req.query.id) {
        id = req.query.id;
    }
    var final = {};
    async.series({
        recipe: function (cb) {
            conn.query("SELECT * FROM przepis, uzytkownicy WHERE przepis.id_user=uzytkownicy.id and przepis.id='" + id + "' limit 1", function (error, result, client) {
                cb(error, result);
            })
        },
        comment: function (cb) {
            conn.query("SELECT * from komentarze, uzytkownicy where id_recipe='" + id + "' and uzytkownicy.id=id_user order by komentarze.insertion_data", function (error, result, client) {
                cb(error, result)
            })
        },
        menu: function (cb) {
            conn.query("select DISTINCT(menu.name), uzytkownicy.username, menu.id_user, menu.id_menu from menu  join  uzytkownicy on ( menu.id_user=uzytkownicy.id) where menu.id_user =" + global.id_user + ";", function (error, result, client) {
                cb(error, result)
            })
        }
    }, function (error, results) {
        let thisUser;

        if (results.recipe[0].id_user == global.id_user) { thisUser = true }
        else { thisUser = false }

        results.comment.forEach(function (el, i) {
            if (el.id_user == global.id_user) { el.is_it_me = true; }
            else { el.is_it_me = false; }
        });

        results.comment.forEach(function (el, i) {
            el.insertion_data = moment(el.insertion_data).format('LLLL')
        });
        res.render('recipe', {
            results: results.recipe,
            comment: results.comment,
            menu: results.menu,
            user: req.session.admin,
            thisUser: thisUser,
            nameOfUser: global.id_username,
        });;
    });
});


app.get('/profile', auth, function (req, res) {
    let id;
    if (req.query.id) {
        id = req.query.id;
    }
    var final = {};
    async.series({
        recipe: function (cb) {
            conn.query("SELECT * FROM przepis where id_user='" + global.id_user + "'", function (error, result, client) {
                cb(error, result);

            })
        },
        user: function (cb) {
            conn.query("SELECT * from  uzytkownicy where  uzytkownicy.id='" + global.id_user + "'", function (error, result, client) {
                cb(error, result)
            })
        }
    },
        function (error, results) {
            if (!error && results.recipe.length == 0) {
                return res.render('profile', { user: req.session.admin, nameOfUser: global.id_username }, function (err, html) {
                    res.send(html + "&nbsp &nbsp &nbsp &nbsp Dotychczas nie dodałeś żadnego przepisu. Aby dodać nowy przepis wejdź w: Dodaj przepis :) ");
                })
            }
            if (!error) {
                res.render('profile', {
                    results: results.recipe,
                    user: results.user,
                    nameOfUser: global.id_username,
                });;
            }
        });
});
//route for likes data 08-04
app.get('/likes', auth, function (req, res) {

    let id_user = `${global.id_user}`;
    let id_rec = req.query.id;
    let sql = "SELECT * FROM polubienia WHERE id_user='" + id_user + "' AND id_recipe = '" + id_rec + "'";
    conn.query(sql, function (error, results, fields) {
        if (typeof results === undefined || results.length == 0) {
            let sql1 = "insert into polubienia values (NULL, " + id_user + ", " + id_rec + ", 0)";
            let query = conn.query(sql1, (err, results2) => {
            });
        }
    });
    let sql2 = "SELECT * FROM polubienia WHERE id_user = '" + id_user + "' AND id_recipe = '" + id_rec + "';";

    conn.query(sql2, function (error, results3, fields) {
        let isliked;
        if (typeof results3 === undefined || results3.length == 0) {
            isliked = 0;
        }
        else {
            isliked = results3[0].czy_polubil;
        }
        if (isliked == 0) {
            let sql3 = "update przepis set likes = likes + 1 where id = '" + id_rec + "';";
            let query2 = conn.query(sql3, (err, results5) => {
            });
            let sql4 = "update polubienia set czy_polubil = 1 where id_user = '" + id_user + "' and id_recipe = '" + id_rec + "';";
            let query3 = conn.query(sql4, (err, results6) => {
            });
        }
        else if (isliked == 1) {
            let slq7 = "update przepis set likes = likes - 1 where id = '" + id_rec + "';";
            let query4 = conn.query(slq7, (err, results7) => {
            });
            let sql8 = "update polubienia set czy_polubil = 0 where id_user = '" + id_user + "' and id_recipe = '" + id_rec + "';";
            let query5 = conn.query(sql8, (err, results8) => {
            });
        }
        else if (isliked == 2) {
            let slq5 = "update przepis set likes = likes + 1, dislikes = dislikes - 1 where id = '" + id_rec + "';";
            let query4 = conn.query(slq5, (err, results7) => {
            });
            let sql6 = "update polubienia set czy_polubil = 1 where id_user = '" + id_user + "' and id_recipe = '" + id_rec + "';";
            let query5 = conn.query(sql6, (err, results8) => {
            });
        }
        res.redirect('/');
    });
});

//route for dislikes data 08-04
app.get('/dislikes', auth, function (req, res) {
    let id_user = `${global.id_user}`;
    let id_rec = req.query.id;
    let sql = "SELECT * FROM polubienia WHERE id_user='" + id_user + "' AND id_recipe = '" + id_rec + "'";
    conn.query(sql, function (error, results, fields) {
        if (typeof results === 'undefined' || results.length == 0) {
            let sql1 = "insert into polubienia values (NULL, " + id_user + ", " + id_rec + ", 0)";
            let query = conn.query(sql1, (err, results2) => {
            });
        }
    });
    let sql2 = "SELECT * FROM polubienia WHERE id_user = '" + id_user + "' AND id_recipe = '" + id_rec + "';";

    conn.query(sql2, function (error, results3, fields) {
        let isliked;
        if (typeof results3 === undefined || results3.length == 0) {
            isliked = 0;
        }
        else {
            isliked = results3[0].czy_polubil;
        }
        if (isliked == 0) {
            let sql3 = "update przepis set dislikes = dislikes + 1 where id = '" + id_rec + "';";
            let query2 = conn.query(sql3, (err, results5) => {
            });
            let sql4 = "update polubienia set czy_polubil = 2 where id_user = '" + id_user + "' and id_recipe = '" + id_rec + "';";
            let query3 = conn.query(sql4, (err, results6) => {
            });
        }
        else if (isliked == 1) {
            let slq5 = "update przepis set likes = likes - 1, dislikes = dislikes + 1 where id = '" + id_rec + "';";
            let query4 = conn.query(slq5, (err, results7) => {
            });
            let sql6 = "update polubienia set czy_polubil = 2 where id_user = '" + id_user + "' and id_recipe = '" + id_rec + "';";
            let query5 = conn.query(sql6, (err, results8) => {
            });
        } else if (isliked == 2) {
            let slq7 = "update przepis set dislikes = dislikes - 1 where id = '" + id_rec + "';";
            let query4 = conn.query(slq7, (err, results7) => {
            });
            let sql8 = "update polubienia set czy_polubil = 0 where id_user = '" + id_user + "' and id_recipe = '" + id_rec + "';";
            let query5 = conn.query(sql8, (err, results8) => {
            });
        }
        res.redirect('/');
    });
});
//ZMIANA add to favourites
app.get('/favourites', (req, res) => {
    global.id_recipe
    id_recipe = req.query.id;
    let sql = `insert into ulubione values (NULL, ${global.id_user}, ${id_recipe})`;
    let query = conn.query(sql, (err, results) => {
        //     //if (err) throw err;
        res.redirect('/');
    });
});
//ZMIANA 01-04
app.get('/favourite', (req, res) => {

    // let id_user = req.query.user;
    let sql = `select ulubione.id_fav, ulubione.id_recipe, nazwa, przygotowanie, skladniki, kategoria, likes, dislikes, photo from przepis, ulubione, uzytkownicy where uzytkownicy.id = ${global.id_user}  and przepis.id=id_recipe and uzytkownicy.id=ulubione.id_user;`;
    let query = conn.query(sql, (err, results) => {
        if (results.length == 0) {
            return res.render('index', { user: req.session.admin, nameOfUser: global.id_username }, function (err, html) {
                res.send(html + "&nbsp &nbsp &nbsp &nbsp Nie posiadasz żadnego przepisu w ulubionych. Aby dodać nowy przepis wejdź na stronę główną i kliknij: Dodaj do ulubionych :) ");
            })
        } else {
            return res.render('favouriteList', {
                results: results,
                nameOfUser: global.id_username
            });
        }
    });
});

app.get('/openAuth', (req, res) => {
    res.render('auth');
});
app.get('/openRegister', (req, res) => {
    res.render('register');
});

//register users-save zmiana 06-05
app.get('/register', (req, res) => {
    let isUserInDatabase;
    let message;
    let message2;
    let message3;
    let sql1 = "select * from uzytkownicy  where username = '" + req.query.username + "' or email=  '" + req.query.email + "'";
    let query1 = conn.query(sql1, (err, results) => {
        if (results[0]) {
            isUserInDatabase = results[0];
        }
        let password = req.query.password;
        let password2 = req.query.password2;
        if (password == password2 && isUserInDatabase === undefined) {
            let data = { username: req.query.username, password: req.query.password, email: req.query.email };
            let sql = "INSERT INTO uzytkownicy SET ?";
            let query3 = conn.query(sql, data, (err, results2) => {
                // if(err) throw err;
                res.redirect('/');
            });
        } else {
            if (isUserInDatabase !== undefined) {
                message2 = "Użytkownik o takim e-mailu/loginie już istnieje!"
            }
            if (password != password2) {
                message3 = "Hasła nie pokrywają się!"
            }
            res.render('register', {
                user: req.session.admin, nameOfUser: global.id_username, message: message, message2: message2, message3: message3
            })
        }
    });
});
//ZMIANA 31-03
app.get('/search', (req, res) => {
    res.render('search', {
        user: req.session.admin,
        nameOfUser: global.id_username
    });
});

//ZMIANA 14-05
app.get('/searchIt', (req, res) => {
    let nazwa = req.query.nazwa;
    let skladniki = req.query.skladniki;
    let antyskladniki = req.query.antyskladniki;
    let kategoriaBody = req.query.kategoria;
    let sql;
    let message = "Wybierz minimum jedno pole!";
    if (nazwa.length == 0 && skladniki.length == 0 && kategoriaBody === undefined && antyskladniki.length == 0) {
        res.render('search', {
            message: message
        });
    } else {
        if (nazwa.length > 0) {
            nazwa = " nazwa like '%".concat(nazwa).concat("%'");
        } else {
            nazwa = "nazwa like '%'";
        }
        if (skladniki.length > 0) {
            let split = skladniki;
            split = split.split(",");
            skladniki = "";
            for (i = 0; i < split.length; i++) {
                if (split[i] == "") {
                    continue;
                }
                skladniki = skladniki.concat(" and skladniki like '%").concat(split[i].concat("%'"));
            }
        }
        if (antyskladniki.length > 0) {
            let split2 = antyskladniki;
            split2 = split2.split(",");
            antyskladniki = "";
            for (i = 0; i < split2.length; i++) {
                if (split2[i] == "") {
                    continue;
                }
                antyskladniki = antyskladniki.concat(" and skladniki not like '%").concat(split2[i].concat("%'"));
            }
        }
        let kategoria;
        if (kategoriaBody !== undefined && Array.isArray(kategoriaBody)) {
            kategoria = " and kategoria in (";
            for (i = 0; i < kategoriaBody.length; i++) {
                if (i < kategoriaBody.length - 1) {
                    kategoria = kategoria.concat("'").concat(kategoriaBody[i]).concat("',");
                }
                else {
                    kategoria = kategoria.concat("'").concat(kategoriaBody[i]).concat("')");
                }
            }
        } else if (kategoriaBody !== undefined) {
            kategoria = " and kategoria like '".concat(kategoriaBody).concat("'");
        } else {
            kategoria = " and kategoria like '%'";
        }
        sql = "SELECT * FROM przepis WHERE " + nazwa + " " + skladniki + " " + antyskladniki + " " + kategoria + "";
        let query2 = conn.query(sql, (err, results) => {
            if (results[0] === undefined) {
                res.render('index', { user: req.session.admin, nameOfUser: global.id_username }, function (err, html) {
                    res.send(html + '&nbsp &nbsp &nbsp &nbsp Niestety nic nie znaleziono. Spróbuj ponownie.');
                })
            }
        });
        let query = conn.query(sql, (err, results) => {
            let namcom;
            if (req.query.nazwa || req.query.skladniki) { namcom = req.query.nazwa + ' ' + req.query.skladniki }
            if (req.query.kategoria.length == 6) {
                req.query.kategoria = "Wszystkie";
            }
            res.render('index', {

                searched: namcom,
                antyskladniki: req.query.antyskladniki,
                kategoriaWyszukiwania: req.query.kategoria,
                results: results,
                user: req.session.admin,
                nameOfUser: global.id_username
            });
        });
    }
});

app.get('/deletefavourite', (req, res) => {
    let id_fav = req.query.id;
    let sql = `delete from ulubione where id_user = ${global.id_user} and id_fav= ${id_fav}`;
    let query = conn.query(sql, (err, results) => {
        res.redirect('/');
    });

});

app.get('/menu', (req, res) => {
    let id_recipe = `${global.id}`;
    let id_user = `${global.id_user}`;
    var posilek = req.query.typposilku;
    let sql = "insert into menu values (NULL,'" + id_user + "','" + id_recipe + "','" + posilek + "')";
    let query = conn.query(sql, (err, results) => {
        res.redirect('/');
    });

});
app.get('/menulist', (req, res) => {
    let search = req.query.search;
    let empty = false;

    if (search == undefined) { search = ""; }


    async.series({
        menu: function (cb) {
            conn.query("SELECT menu.id_menu, przepis.photo, menu.name, uzytkownicy.username, menu.id_user, menu.access from menu join uzytkownicy on (menu.id_user = uzytkownicy.id) join menuitem on (menu.id_menu = menuitem.id_menu) join przepis on (menuitem.id_recipe = przepis.id)  WHERE menu.name LIKE  '%" + search + "%' group by menu.id_menu;", function (error, result, client) {
                cb(error, result)//

            })
        }
    },
        function (error, results) {
            results.menu.forEach(function (el, i) {
                if (el.id_user == global.id_user) { el.access = true; }
                else if (el.access == "public") { el.access = true; }
                else { el.access = false; }
            });

            if (!results.menu[0]) { empty = true }

            if (!error) {
                res.render('menu', {
                    menu: results.menu,
                    nameOfUser: global.id_username,
                    menuSearch: true,
                    empty: empty,
                    searched: search
                });;
            }
        });

});
app.get('/addToMenuItem', (req, res) => {

    let sql = "INSERT INTO menuItem SET ?";
    let data = { id_menu: req.query.id_menu, type: req.query.type, day: req.query.day, id_recipe: global.id };
    let query = conn.query(sql, data, (err, results) => { });
    res.redirect('/recipe');
});

app.get('/addToMenuItem2', (req, res) => {

    let sql = "INSERT INTO menuItem SET ?";
    let data = { id_menu: req.query.id, type: req.query.type, day: req.query.day, id_recipe: req.query.id_menu };
    let query = conn.query(sql, data, (err, results) => { });
    res.redirect('/menuList');
});
app.get('/addToMenu', (req, res) => {

    let sql = "INSERT INTO menu SET ?";
    let data = { id_user: global.id_user, name: req.query.name, access: req.query.access };
    let query = conn.query(sql, data, (err, results) => { });
    res.redirect('/menuList');
});
app.get('/deleteMenu', (req, res) => {

    let sql = "DELETE FROM menu WHERE id_menu=" + req.query.id + "";
    let query = conn.query(sql, (err, results) => {
        res.redirect('/menuList');
    });
});
app.get('/editMenu', (req, res) => {

    let sql = "UPDATE menu SET name='" + req.query.name + "' WHERE id_menu=" + req.query.id;
    let query = conn.query(sql, (err, results) => {
        res.redirect('/menuList');
    });
});
app.get('/deleteMenuItem', (req, res) => {
    let sql = "DELETE FROM menuitem WHERE id_menu_item=" + req.query.id + "";
    let query = conn.query(sql, (err, results) => {
        res.redirect('/menuList');
    });
});
app.get('/openMenuItem', (req, res) => {

    async.series({
        poniedzialek: function (cb) {
            conn.query("SELECT * FROM menuitem, menu, przepis WHERE menuitem.id_menu='" + req.query.id_menu + "' and menuitem.id_recipe = przepis.id and menu.id_menu=menuitem.id_menu and menuitem.day='Poniedziałek';", function (error, result, client) {
                cb(error, result);
            })
        },
        wtorek: function (cb) {
            conn.query("SELECT * FROM menuitem, menu, przepis WHERE menuitem.id_menu='" + req.query.id_menu + "' and menuitem.id_recipe = przepis.id and menu.id_menu=menuitem.id_menu and menuitem.day='Wtorek';", function (error, result, client) {
                cb(error, result);
            })
        },
        sroda: function (cb) {
            conn.query("SELECT * FROM menuitem, menu, przepis WHERE menuitem.id_menu='" + req.query.id_menu + "' and menuitem.id_recipe = przepis.id and menu.id_menu=menuitem.id_menu and menuitem.day='Środa';", function (error, result, client) {
                cb(error, result);
            })
        },
        czwartek: function (cb) {
            conn.query("SELECT * FROM menuitem, menu, przepis WHERE menuitem.id_menu='" + req.query.id_menu + "' and menuitem.id_recipe = przepis.id and menu.id_menu=menuitem.id_menu and menuitem.day='Czwartek';", function (error, result, client) {
                cb(error, result);
            })
        },
        piatek: function (cb) {
            conn.query("SELECT * FROM menuitem, menu, przepis WHERE menuitem.id_menu='" + req.query.id_menu + "' and menuitem.id_recipe = przepis.id and menu.id_menu=menuitem.id_menu and menuitem.day='Piątek';", function (error, result, client) {
                cb(error, result);
            })
        },
        sobota: function (cb) {
            conn.query("SELECT * FROM menuitem, menu, przepis WHERE menuitem.id_menu='" + req.query.id_menu + "' and menuitem.id_recipe = przepis.id and menu.id_menu=menuitem.id_menu and menuitem.day='Sobota';", function (error, result, client) {
                cb(error, result);
            })
        },
        niedziela: function (cb) {
            conn.query("SELECT * FROM menuitem, menu, przepis WHERE menuitem.id_menu='" + req.query.id_menu + "' and menuitem.id_recipe = przepis.id and menu.id_menu=menuitem.id_menu and menuitem.day='Niedziela';", function (error, result, client) {
                cb(error, result);
            })
        },
        recipe: function (cb) {
            conn.query("SELECT * FROM przepis;", function (error, result, client) {
                cb(error, result);
            })
        },
    }, function (error, results) {

        if (!error) {

            res.render('menuItem', {
                recipe: results.recipe,
                poniedzialek: results.poniedzialek,
                wtorek: results.wtorek,
                sroda: results.sroda,
                czwartek: results.czwartek,
                piatek: results.piatek,
                sobota: results.sobota,
                niedziela: results.niedziela,
                menuName: req.query.name,
                menuId: req.query.id_menu,
                nameOfUser: global.id_username,
                menuSearch: true
            });;
        }
    });
});
app.listen(3000);
